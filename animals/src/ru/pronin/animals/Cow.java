package ru.pronin.animals;

/**
 * Created by мир техники on 21.10.2014.
 */
public class Cow implements Animals  {

    @Override
    public String getName() {
        return "Корова";
    }

    @Override
    public String getVoice() {
        return "mu mu";
    }
}

