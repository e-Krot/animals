package ru.pronin.animals;

/**
 * Created by мир техники on 16.10.2014.
 */
public class Main  {
    public static void main(String[] args) {
        Dog dog = new Dog();

        Cat cat = new Cat();

        print(dog);
        
        print(cat);

    }

    private static void print (Animals animals) {
        String str =
                animals.getName() +
                        "говорит" +
                        animals.getVoice();
        System.out.println(str);
    }

}